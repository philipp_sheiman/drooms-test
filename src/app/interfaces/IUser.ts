export interface IUser {
    node: {
        name: string;
        avatarUrl: string;
        email: string;
        login: string;
        id: string;
        createdAt: string;
        url: string;
        websiteUrl: string;
    }
}