import {IUser} from './IUser';

export interface IUserResponse {
    data: {
        search: {
            pageInfo: {
                endCursor,
                hasNextPage
            },
            edges: IUser[]
        }
    }
}