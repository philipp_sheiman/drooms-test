import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import _debounce from 'lodash/debounce'

import { UserService } from './../../services/user.service';
import { IUser } from './../../interfaces';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {
  @Input() searchQuery: string

  page = 1;
  itemsPerPage = 10;
 
  users: IUser[] = [];
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.connect()
      .subscribe(response => {
        this.users = response;
      })
  }

  loadMoreUsers(lower: number, upper: number): void {
    this.userService.getUsersByPosition(lower, upper);
  }

  onPageChange(): void {
    this.loadMoreUsers((this.page - 1) * this.itemsPerPage, ((this.page) * this.itemsPerPage));
  }

  onUserSelected(user: IUser): void {
    this.router.navigate(['/user', { ...user.node }] )
  }

  resetSearch(): void {
    this.userService.searchUsers(null);
    this.searchQuery = ''
  }

  searchUsers(): void {
    const debounceSearch = _debounce(this.userService.searchUsers, 200);
    return debounceSearch(this.searchQuery)
  }

}
