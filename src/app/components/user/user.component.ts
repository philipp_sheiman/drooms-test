import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IUser } from '../../interfaces';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent {
  @Input() public user: IUser;
  @Output() selected = new EventEmitter<IUser>();

  onUserSelected(): void {
    this.selected.emit(this.user);
  }
}