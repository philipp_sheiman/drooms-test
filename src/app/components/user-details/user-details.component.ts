import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IUser } from 'src/app/interfaces';


@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.sass']
})
export class UserDetailsComponent implements OnInit {

  user: IUser;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = <IUser>{ node: { ...this.route.snapshot.params } }
  }

}
