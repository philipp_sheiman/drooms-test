import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import _ from 'lodash';

import { IUserResponse, IUser } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  githubUrl = 'https://api.github.com/graphql'

  token = '1e8bd4856abb755117919f1f0025b263d2345694'

  users: IUser[] = [];

  searchIndexes = []

  universalOutputObservable: BehaviorSubject<IUser[]> = new BehaviorSubject([]);

  private mutation(endCursor?: string): string {
    return `
    {
      search(query: "type:user", ${endCursor ? 'after: "' + endCursor + '",' : ''} first: 100, type: USER) {
        userCount
        pageInfo {
          endCursor
          hasNextPage
        }
        edges {
          node {
            ... on User {
              name
              avatarUrl
              email
              login
              id
              createdAt
              url
              websiteUrl
            }
          }
        }
      }
    }
  `};

  constructor(
    private http: HttpClient
  ) { 
    this.getUsers()
    this.searchUsers = this.searchUsers.bind(this);
  }

  public connect(): BehaviorSubject<IUser[]> {
    if (this.users.length === 1000) {
      this.universalOutputObservable.next(this.users);
    }
    return this.universalOutputObservable;
  }

  public getUsersByPosition(start: number, end: number): void {
    return this.universalOutputObservable.next(this.users.slice(start, end));
  }

  public searchUsers(searchQuery: string): void {
    if (!searchQuery) {
      this.universalOutputObservable.next(this.users.slice(0, 10));
      return;
    };
    const foundItems = this.searchIndexes.map(node => {
      return node.target.includes(searchQuery) ? node : null
    });
    const results = _.compact(foundItems).map(item => (
      this.users[item.id]
    ));
    this.universalOutputObservable.next(results)
  }

  transformToSearchIndex(edges) {
    this.searchIndexes = [];
    _.transform(edges, (result, {node}, index) => {
      this.searchIndexes.push({
        id: index,
        target: [node.name, node.email, node.login].join(',').toLowerCase()
      })
    }, {})
  }
  
  public getUsers(endCursor?: string): void {
    if (this.users.length === 1000) return;

    let headers = new HttpHeaders();
    headers = headers.append('Authorization', `bearer ${this.token}`);
    this.http.post(this.githubUrl, { query: this.mutation(endCursor && endCursor) }, { headers })
      .subscribe((response: IUserResponse) => {
        const { edges, pageInfo: { endCursor } } = response.data.search;

        if (this.users.length === 0) {
          this.universalOutputObservable.next(<IUser[]>edges.slice(0, 10));
        }
        this.users = this.users.concat(edges);

        this.transformToSearchIndex(this.users)

        this.getUsers(endCursor)
      });
  }
}