import { TestBed, getTestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

const DATA = [
  {
    "node": {
      "name": "Linus Torvalds",
      "avatarUrl": "https://avatars0.githubusercontent.com/u/1024025?v=4",
      "createdAt": "2011-09-03T15:26:22Z",
      "url": "https://github.com/torvalds",
      "websiteUrl": null,
      "email": "",
      "login": "torvalds",
      "id": "MDQ6VXNlcjEwMjQwMjU="
    }
  },
  {
    "node": {
      "name": "Jake Wharton",
      "avatarUrl": "https://avatars0.githubusercontent.com/u/66577?v=4",
      "createdAt": "2009-03-24T16:09:53Z",
      "url": "https://github.com/JakeWharton",
      "websiteUrl": "http://jakewharton.com",
      "email": "jakewharton@gmail.com",
      "login": "JakeWharton",
      "id": "MDQ6VXNlcjY2NTc3"
    }
  },
  {
    "node": {
      "name": "Ruan YiFeng",
      "avatarUrl": "https://avatars0.githubusercontent.com/u/905434?v=4",
      "createdAt": "2011-07-10T01:07:17Z",
      "url": "https://github.com/ruanyf",
      "websiteUrl": "https://twitter.com/ruanyf",
      "email": "yifeng.ruan@gmail.com",
      "login": "ruanyf",
      "id": "MDQ6VXNlcjkwNTQzNA=="
    }
  },
  {
    "node": {
      "name": "Evan You",
      "avatarUrl": "https://avatars1.githubusercontent.com/u/499550?v=4",
      "createdAt": "2010-11-28T01:05:40Z",
      "url": "https://github.com/yyx990803",
      "websiteUrl": "http://evanyou.me",
      "email": "",
      "login": "yyx990803",
      "id": "MDQ6VXNlcjQ5OTU1MA=="
    }
  },
  {
    "node": {
      "name": "TJ Holowaychuk",
      "avatarUrl": "https://avatars2.githubusercontent.com/u/25254?v=4",
      "createdAt": "2008-09-18T22:37:28Z",
      "url": "https://github.com/tj",
      "websiteUrl": "https://apex.sh",
      "email": "tj@apex.sh",
      "login": "tj",
      "id": "MDQ6VXNlcjI1MjU0"
    }
  },
  {
    "node": {
      "name": "Dan Abramov",
      "avatarUrl": "https://avatars0.githubusercontent.com/u/810438?v=4",
      "createdAt": "2011-05-25T18:18:31Z",
      "url": "https://github.com/gaearon",
      "websiteUrl": "http://twitter.com/dan_abramov",
      "email": "dan.abramov@me.com",
      "login": "gaearon",
      "id": "MDQ6VXNlcjgxMDQzOA=="
    }
  },
  {
    "node": {
      "name": "Addy Osmani",
      "avatarUrl": "https://avatars2.githubusercontent.com/u/110953?v=4",
      "createdAt": "2009-08-01T18:39:25Z",
      "url": "https://github.com/addyosmani",
      "websiteUrl": "https://www.addyosmani.com",
      "email": "addyosmani@gmail.com",
      "login": "addyosmani",
      "id": "MDQ6VXNlcjExMDk1Mw=="
    }
  },
  {
    "node": {
      "name": "Sindre Sorhus",
      "avatarUrl": "https://avatars1.githubusercontent.com/u/170270?v=4",
      "createdAt": "2009-12-20T22:57:02Z",
      "url": "https://github.com/sindresorhus",
      "websiteUrl": "https://sindresorhus.com",
      "email": "sindresorhus@gmail.com",
      "login": "sindresorhus",
      "id": "MDQ6VXNlcjE3MDI3MA=="
    }
  },
  {
    "node": {
      "name": "Siraj Raval",
      "avatarUrl": "https://avatars3.githubusercontent.com/u/1279609?v=4",
      "createdAt": "2011-12-22T09:57:32Z",
      "url": "https://github.com/llSourcell",
      "websiteUrl": "www.youtube.com/c/sirajraval",
      "email": "",
      "login": "llSourcell",
      "id": "MDQ6VXNlcjEyNzk2MDk="
    }
  },
  {
    "node": {
      "name": "Paul Irish",
      "avatarUrl": "https://avatars0.githubusercontent.com/u/39191?v=4",
      "createdAt": "2008-12-09T00:39:23Z",
      "url": "https://github.com/paulirish",
      "websiteUrl": "http://paulirish.com",
      "email": "",
      "login": "paulirish",
      "id": "MDQ6VXNlcjM5MTkx"
    }
  }
]

describe('UserService', () => {
  let service: UserService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    injector = getTestBed();
    service = injector.get(UserService);
    httpMock = injector.get(HttpTestingController); 
  })

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('should find user', () => {
    let outputObservable = service.connect();
    let req = httpMock.expectOne('https://api.github.com/graphql');

    outputObservable.subscribe(data => {
      console.log(data);
    })

    req.flush(DATA);
  })
});
